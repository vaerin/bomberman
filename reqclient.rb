require 'rubygems'
require 'ffi-rzmq'

context = ZMQ::Context.new(1)

# Socket to talk to server
puts "Connecting to hello world server..."
requester = context.socket(ZMQ::REQ)
requester.connect("tcp://localhost:1234")

0.upto(9) do |request_nbr|
  puts "Rejestracja gracza 1 #{request_nbr}..."
  requester.send_string "Gracz 1 #{request_nbr}"
  reply = ''
  rc = requester.recv_string(reply)
  
  puts "Odp #{request_nbr}: [#{reply}]"
  sleep 1
end
require 'rubygems'; 
require 'json/pure'; 

##puts JSON.load('[421212]').inspect

class MSGS

	def initialize
		@alias = 'marcin'
		@id = 1
		@start_time = Time.now.to_i
	end

	def msg (options)
		time =  Time.now.to_i - @start_time if @start_time
		message = Hash.new
		data = Hash.new
		type = options[:type]
		
		["move_begin","move_end","no_change","bomb","die"].each do |x| 
			if x == type
				@category= "game_logic"
				
			end
		end
		
		["ready", "listening","withdraw_game"].each do |x|
			if x == type	
				@category = "game_state"
			end
		end
		
		message[:category] = @category
		message[:sender] = @alias
		data[:type] = type
		data[:player_id] = @id
		data[:time_real] = time if time
		message[:data] = data
		
		data[:start_field] = options[:sf] if options[:sf]
		data[:end_field] = options[:ef] if options[:ef] 
		data[:field] = options[:f] if options[:f]
		data[:killer_id] = options[:killer] if options[:killer]
			
		
		j_msg = JSON.generate(message)
		return j_msg
	end
	
	def read_msg (j_msg)
		msg = JSON.parse j_msg
			case msg["data"]["type"]
				when "move_begin"
					puts "move_begin #{msg["data"]["start_field"]} -> #{msg["data"]["end_field"]}"
				when "no_change"
					puts "nochange"
				when "bomb"
					puts "bomb"
				when "die"
					puts "die"
				when "ready"
					puts "ready"
				when "listen"
					puts "listen"
			end
	end
	
end

m = MSGS.new
sleep 1
puts m.msg(:type => 'ready')
sleep 1
puts m.msg(:type => 'listening')
sleep 1
ms =  m.msg(:type => 'move_begin',:sf => [1,1], :ef => [1,2])
puts ms
sleep 1
puts m.msg(:type => 'die', :killer => 2)

m.read_msg(ms)


# puts 'ok'
# ms = m.msg(:type => 'new')
# puts ms
# lol = JSON.parse ms
# puts lol.class
# p lol["data"]["time"]
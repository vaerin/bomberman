require 'rubygems'
require 'ffi-rzmq'
 
class Subscriber
  attr_reader :id, :socket
  def initialize(id = nil)
    @id = id || rand.to_s
  end
 
  def connect(*addrs)
    if !@socket
      context = addrs.shift
      @socket = context.socket ZMQ::SUB
      @socket.setsockopt ZMQ::IDENTITY, @id
    end
 
    addrs.each do |addr|
      @socket.connect addr
    end
  end
 
  def subscribe_to(*channels)
    channels.each { |ch| @socket.setsockopt ZMQ::SUBSCRIBE, ch }
  end
 
  def process(line = nil)
    #sline ||= @socket.recv
	line = ''
    socket.recv_string(line)
    chan, user, msg = line.split ' ', 3
    puts "##{chan} [#{user}]: #{msg}"
    true
  rescue SignalException
    process(line) if line
    false
  end
 
  def close
    @socket.close
    @socket = nil
  end
end
 
subscriber = Subscriber.new ARGV[0]
subscriber.connect ZMQ::Context.new, 'tcp://127.0.0.1:5555'
subscriber.subscribe_to 'rubyonrails', 'ruby-lang', 'ping'
 
loop do
  unless subscriber.process
    subscriber.close
    puts "Quitting..."
    exit
  end
end
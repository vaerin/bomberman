﻿require 'rubygems'
require 'ffi-rzmq'


context = ZMQ::Context.new
pub = context.socket ZMQ::PUB
pub.setsockopt ZMQ::IDENTITY, 'ping-pinger'
pub.setsockopt ZMQ::RECOVERY_IVL, 20
pub.bind 'tcp://*:5555'
 
i=0
loop do
  ping = "ping pinger #{i+=1}" ;
  pub.send_string(ping)
  sleep 1
end
﻿# lines = open('master.ini').lines
# lines each do |x|
# parts = x.split('=')
# case part[0]
	# when 'port' then port = part[1]
	# when 'addres' then addr = part[1]
	# when 'addres2' then addr = part[2]
# end

#!/usr/bin/env ruby
require 'rubygems' rescue nil
require 'chingu'
require 'ffi-rzmq'
require 'JSON/pure'
include Gosu



class Game < Chingu::Window
  def initialize
    super(17*20,17*20)
	role = ARGV[0]
	$context = ZMQ::Context.new(1)
	if role == 'Master'
		push_game_state(Master)
	else
		push_game_state(Slave)
	end
  end
end


#
# GAMESTATE #1 - Master
#
class Master < Chingu::GameState 
	def initialize(options = {})
		super
		@title = Chingu::Text.create(:text=>"Lista Graczy", :x=>10, :y=>10, :size=>30)
		self.input = { :f1 => :next_step, :f2 => next_step}
		@players = Array.new 
		@current_y = 45
		@gap = 20
		@current_x = 10
		@lines = 1
		#socket
		@socket = $context.socket(ZMQ::REP)
		@socket.bind("tcp://*:1234")
		#wątek od czytania
		@thr = Thread.new{register_player}
	end
	
	def register_player
		continue = true
		while continue do
			request = ''
			rc = @socket.recv_string(request,ZMQ::NOBLOCK)
			if request != ''
				@socket.send_string("ok")
				@players << request
			end
			sleep 0.1				
		end 
	end
	
	def update
		super
		if @players.count > 0
			player = @players[0]
			add_player(player)
			@players.shift
			if @lines == 12
				@lines = 1
				@current_x += 150
			else
				@lines += 1
			end
		end
	end
	
	
	def add_player (msg)
		txt = Chingu::Text.create(:text=> msg, :x=> @current_x, :y => @current_y+ @gap*@lines , :size=>15 )
		puts msg
	end
  
	def next_step
		#@thr.exit
		#@socket.close
		
		Slave
	end
	
	def finalize
		@thr.exit
		@socket.close
	end
end

#
# GAMESTATE #2 - Slave
#
class Slave < Chingu::GameState
	trait :timer
  def initialize(options = {})
    super
    @title = Chingu::Text.create(:text => "Czekam na mastera ...", :x=>10, :y=>10, :size=>20)
	after(100){
		requester = $context.socket(ZMQ::REQ)
		requester.connect("tcp://localhost:1234")
		requester.send_string "Gracz"
		reply = ''
		rc = requester.recv_string(reply)
		@title.text = "Gracz zarejestrowany. Czekam na start"
		requester.close
	}
  end
  
  def get_master_publisher (reply)
		msg =  JSON.parse(reply)
		msg["data"][""]
  end
  
  def get_player_list
	
  end
  
  def next_step
	Master
  end
end

class Gameplay < Chingu::GameState
	def initialize(options = {})
		super
		self.input = { :n => next_step }
	end


    #@title = Chingu::Text.create(:text=>"Level #{options[:level].to_s}. P: pause R:restart", :x=>20, :y=>10, :size=>30)
  

end

class Gameplay1 < Chingu::GameState
	def initialize(options = {})
		super
		self.input = { :n => next_step }
	end


    #@title = Chingu::Text.create(:text=>"Level #{options[:level].to_s}. P: pause R:restart", :x=>20, :y=>10, :size=>30)
  

end

Game.new.show
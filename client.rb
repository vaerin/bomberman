﻿# encoding: utf-8
require 'rubygems'
require 'ffi-rzmq'

COUNT = 100

context = ZMQ::Context.new(1)

# Socket to talk to server
puts "Collecting updates from weather server"
subscriber = context.socket(ZMQ::SUB)
subscriber.connect("tcp://localhost:555")

# Subscribe to zipcode, default is NYC, 10001
filter = ARGV.size > 0 ? ARGV[0] : ""
subscriber.setsockopt(ZMQ::SUBSCRIBE, filter)

# Process 100 updates
total_temp = 0
1.upto(COUNT) do 
  s = ''
  subscriber.recv_string(s)#.split.map(&:to_i)
  puts s

  end


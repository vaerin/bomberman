#!/usr/bin/env ruby
require 'rubygems' rescue nil
require 'chingu'
require 'ffi-rzmq'
require 'json/pure' 
include Gosu
include Chingu


class Game < Chingu::Window

	attr_accessor :map_name, :start, :alias, :enemies, :port, :ip, :id, :result, :enemies_count
	
	def initialize
		super(17*20,17*20)
		@result = false
		@enemies_count = 0
		@map_name = "1.map"
		@ip = '192.168.1.12'
		self.input = {:esc => :exit}
		p role = ARGV[0]
		unless role == "Master" or role == "Slave"
			raise "Nie podano roli lub wpisano zla nazwe"
		end
		$context = ZMQ::Context.new(1)
		if role == 'Master'
			p @alias = 'MMaster'
			@port = 555
			@start= [0,0]
			@id = 0
		elsif role == 'Slave'
			if ARGV[1]
				mdf = ARGV[1].to_i
			else
				mdf = rand(23..100)
			end
			@port = 557 + mdf
			p @alias = "MSlave #{mdf}"
		end
		$publisher = $context.socket(ZMQ::PUB)
		$publisher.bind("tcp://*:#{@port}")
		if role == 'Master'
			push_game_state(Master)
		elsif role == 'Slave'
			push_game_state(Slave)
		end
   end
  
	def update
		super
		self.caption = "Bomberman! - #{self.fps}"
	end
  
    def draw
		fill(Gosu::Color::GREEN)
		super
	
	end

	def close
		puts 'bye'
		super
	end
	
end

#
# GAMESTATE #1 - Master
#
class Master < Chingu::GameState 

	def initialize(options = {})
		super
		@title = Chingu::Text.create(:text=>"Lista Graczy", :x=>10, :y=>10, :size=>30)
		@player_buffer = Array.new
		@players_list = Array.new
		add_me
		@current_y = 45
		@gap = 20
		@current_x = 10
		@lines = 1
		@reply = generate_reply
		#socket
		@socket = $context.socket(ZMQ::REP)
		@socket.bind("tcp://*:1234")
		#wątek od czytania
		@thr = Thread.new{register_player}
		self.input = { :f1 => :next_gamestate, :f2 => Gameplay}
	end
	
	def register_player
		continue = true
		while continue do
			request = ''
			rc = @socket.recv_string(request,ZMQ::NOBLOCK)
			if request != ''
				@socket.send_string(@reply)
				@player_buffer << request
			end
			sleep 0.1				
		end 
	end

	def generate_reply
		msg = Hash.new
		msg['category'] = 'negotiations'
		data = Hash.new
		data['type'] = 'reply'
		data['player_id'] = 0
		data['IP'] = $window.ip
		data['port'] = $window.port
		msg['data'] = data
		j_msg = JSON.generate(msg)
		return j_msg
	end
	
	def update
		super
		if @player_buffer.count > 0
			player = @player_buffer[0]
			add_player(player)
			@player_buffer.shift
			if @lines == 12
				@lines = 1
				@current_x += 150
			else
				@lines += 1
			end
		end
	end
	
	def add_player (j_msg)
		msg = JSON.parse(j_msg)
		txt = Chingu::Text.create(:text=> msg['data']['name'], :x=> @current_x, :y => @current_y+ @gap*@lines , :size=>15 )
		id = @players_list.count
		if id < 12
			player = Hash.new
			player['name'] = msg['data']['name']
			player['player_id'] = id
			player['IP'] = msg['data']['IP']
			player['port'] = msg['data']['port']
			@players_list << player
		end
	end
	
	def add_me
		player = Hash.new
		player['name'] = $window.alias
		player['player_id'] = $window.id
		player['IP'] = $window.ip
		player['port'] = $window.port
		@players_list << player
	end

	def send_player_list
		size = @players_list.count
		$window.map_name  = case size
			when 2..4 then "1.map"
			when 5..8 then "2.map"
			when 8..12 then "3.map"
		end
		p $window.map_name
		lins = open($window.map_name).lines
		spawnpoints = Array.new
		lins.each do |x| 
			values = x.split(' ')
			if values[2] == '2'
				field = [values[0].to_i, values[1].to_i]
				spawnpoints << field
			end 
		end
		last_field = 0 
		@players_list.each do |x|
			field = spawnpoints[last_field]
			x['field'] = field
			last_field += 1
		end
		msg = Hash.new
		msg['category'] = 'game_state'
		data = Hash.new 
		data['type'] = 'start_info'
		data['map'] = $window.map_name
		data['players'] = @players_list
		msg['data'] = data
		j_msg = JSON.generate(msg)
		#p j_msg
		p $window.enemies = @players_list
		$publisher.send_string(j_msg)
	end
  
	def next_gamestate
		if(@players_list.count > 1)
			@thr.exit
			@socket.close
			@continue = false
			sleep 1
			send_player_list
			p "Jedziem dalej"
			Gameplay
		end
	end

	def finalize
		@thr.exit
		@socket.close
	end
end

#
# GAMESTATE #2 - Slave
#
class Slave < Chingu::GameState
	trait :timer
	def initialize(options = {})
		super
		@title = Chingu::Text.create(:text => "Czekam na mastera ...", :x=>10, :y=>10, :size=>20)
		after(100){
			requester = $context.socket(ZMQ::REQ)
			#todo zmiana adresu ip
			requester.connect("tcp://localhost:1234")
			#todo komunikat
			requester.send_string generate_request
			reply = ''
			rc = requester.recv_string(reply)
			@title2 = Chingu::Text.create(:text => "Gracz zarejestrowany. Czekam na start", :x=>10, :y=>35, :size=>20)
			requester.close
			get_master_publisher(reply)
			listener = $context.socket(ZMQ::SUB)
			link = "tcp://#{@ip}:#{@port}"
			listener.connect(link)
			listener.setsockopt(ZMQ::SUBSCRIBE, "")
			p "Socket SUB ok #{link}"
			while true do
				@raw_player_list = ''
				listener.recv_string(@raw_player_list,ZMQ::NOBLOCK)
				if @raw_player_list != ''
					break
					p @raw_player_list 
				end
				sleep 0.1
			end
			@title2 = Chingu::Text.create(:text => "Otrzymano liste graczy", :x=>10, :y=>60, :size=>20)
			get_player_list(@raw_player_list)
		}.then{Gameplay}
		self.input = { :f1 => Gameplay}
	end

  	def generate_request
		msg = Hash.new
		msg['category'] = 'negotiations'
		data = Hash.new
		data['type'] = 'request'
		data['name'] = $window.alias
		data['IP'] = $window.ip
		data['port'] = $window.port
		msg['data'] = data
		j_msg = JSON.generate(msg)
		return j_msg
	end
  
  def get_master_publisher (reply)
		msg =  JSON.parse(reply)
		@ip = msg["data"]["IP"]
		@port = msg["data"]["port"]

  end
  
  def get_player_list (raw_player_list)
		msg = JSON.parse(raw_player_list)
		list = msg["data"]["players"]
		list.each do |x|
			if x['name'] == $window.alias
				$window.id = x['player_id']
				$window.start = x['field']
				break
			end
		end
		$window.enemies = list
		$window.map_name = msg["data"]["map"]
		
  end
end

class Gameplay < Chingu::GameState
	def initialize(options = {})
		super
		#self.input = { :n => next_step }
		$start_time  = Time.now.to_i
		$map = Map.new(:map => $window.map_name);
		$player = Player.create(:x => $window.start[0], :y => $window.start[1], :name => $window.alias,
			 :id => $window.id, :port => $window.port);
		add_enemies($window.enemies) 
	end

	def add_enemies(enemy_list)
		enemy_list.each do |enemy|
			p enemy
			if enemy['player_id'] != $window.id
				x = enemy['field'][0]
				y = enemy['field'][1]
				id = enemy['id']
				port = enemy['port']
				ip = enemy['IP']
				en = Enemy.create(:x => x, :y => y, :id => id, :port => port, :ip => ip)
				$window.enemies_count +=1
			end
		end
	end
end



class Player < GameObject

	trait :timer
	attr_accessor :alive 
	
	def initialize(options)
		super
		#ruchy
		self.input = {  :left => :left, :right => :right, :up => :up, :down => :down, :space => :plant }
		#miesjce x,y - dokladne dx,dy - na siatce
		@alias = options[:name]
		@player_id = options[:id]
		@x = options[:x] * 20
		@bombx = @gx = options[:x]
		@y = options[:y] * 20
		@bomby = @gy = options[:y]
		#dane dla bomby
		@alive = true
		@moving = false
		@bomb = Bomb.create(:x => -200, :y => -200);
		#inicjacja publishera - tylko gracz
		#pierwsza wiadomosc
		@last_update = Time.now.to_i
		msg(:type => 'no_change')
	end
	
	def update
		super
		if Time.now.to_i - @last_update  > 1.5
			msg(:type => 'no_change')
		end
	end
	
	#zabicie gracza
	def kill
		p "You are dead! Not big suprise!"
		msg(:type => 'die')
		Chingu::Text.create(:text => "Przegrana", :x=>53, :y=>120, :size=>50, :color => Color::BLACK) unless $window.result
		$window.result = true
		@alive = false
	end
	
	#czy mozna wykonac ruch
	def legal_move? dir
		nx = @gx  
		ny = @gy
		case dir
					when 'left' 	then nx -= 1
					when 'right' 	then nx += 1
					when 'up' 		then ny -= 1
					when 'down' 	then ny += 1
				end
		#nie wychodzimy za mape ani na przeszkody
		if nx < 0 or ny < 0 or nx > $map.max_y or ny > $map.max_y or $map.obstacle?(nx,ny)
			return false
		else
			return true
		end
	end
	
	#idz w okreslona strone
	def move dir
		if legal_move?(dir) and !@moving and @alive 
			@moving = true
			tmp_x = x
			tmp_y = y
			speed = 0.66
		    interval = 20
			nx = @gx
			ny = @gy
			case dir
					when 'left' 	then nx -= 1
					when 'right' 	then nx += 1
					when 'up' 		then ny -= 1
					when 'down' 	then ny += 1
			end
			msg(:type => "move_begin", :sf => [@gx,@gy], :ef => [nx,ny])
			after(250){
				@bombx = nx 
				@bomby = ny 
			}
			during(500){
				case dir
					when 'left' 	then @x -= speed
					when 'right' 	then @x += speed
					when 'up' 		then @y -= speed
					when 'down' 	then @y += speed
				end
				}.then {
				msg(:type => "move_end", :sf => [@gx,@gy], :ef => [nx,ny])
				case dir
					when 'left' 	
						@x = tmp_x - interval
						@gx -= 1
					when 'right' 	
						@x = tmp_x + interval
						@gx += 1
					when 'up' 		
						@y = tmp_y - interval 
						@gy -= 1
					when 'down' 
						@y = tmp_y + interval;
						@gy += 1
				end
				@moving = false
				}
		end
	end

	##
	# "Wejœcia" dla ruchów
	##
	
	def left
		move 'left'
	end
  
	def right
		move 'right'
	end
	  
	def up
		move 'up'
	end

	def down
		move 'down'
	end	
		
	#podlozenie bomby	
	def plant
		@bomb.plant(@gx,@gy,@player_id) 
	end
	
	def bomb_planted
		msg(:type => "bomb", :f => [@gx,@gy]) 
	end
	
	def id
		@player_id
	end
	
	#czy gracz jest w zasiegu bomby
	def in_range? (x,y)
		x == @bombx and y  == @bomby
	end
	
	#wysylanie wiadomoœci
	def msg (options)
		time =  Time.now.to_i - $start_time 
		message = Hash.new
		data = Hash.new
		type = options[:type]
		
		["move_begin","move_end","no_change","bomb","die"].each do |x| 
			if x == type
				@category= "game_logic"
			end
		end
		
		["withdraw_game"].each do |x|
			if x == type	
				@category = "game_state"
			end
		end
		
		message[:category] = @category
		message[:sender] = @alias
		data[:type] = type
		data[:player_id] = @player_id
		data[:time_real] = time
		message[:data] = data
		
		data[:start_field] = options[:sf] if options[:sf]
		data[:end_field] = options[:ef] if options[:ef] 
		data[:field] = options[:f] if options[:f]
		data[:killer_id] = options[:killer] if options[:killer]
			
		
		j_msg = JSON.generate(message)
		$publisher.send_string j_msg if @alive and $window.result == false
		@last_update = Time.now.to_i
	end

	#rysowania gracza na mapie
	def draw
		Image["img/bomber.png"].draw(@x,@y,2) if @alive
	end
end

class Enemy < GameObject

	def initialize(options)
		super
		#miesjce x,y - dokladne dx,dy - na siatce
		@x = options[:x] * 20
		@gx = options[:x]
		@y = options[:y] * 20
		@gy = options[:y]
		@id = options[:id]
		port = options[:port]
		ip = options[:ip]
		#czy zyje?
		@alive = true
		@last_ping = 0
		#bomba
		@bomb = Bomb.create(:x => -200, :y => -200);
		@bomb_list = Array.new
		#lista ruchów
		@move_list = Array.new
		@moving = false
		@waiting = false
		#inicjalizacja subscrybenta
		@subscriber = $context.socket(ZMQ::SUB)
		@subscriber.connect("tcp://#{ip}:#{port}")
		@subscriber.setsockopt(ZMQ::SUBSCRIBE, '')
		
		@ready = false

		@listener = Thread.new{listen}
		@pusher = Thread.new{next_move}
		@bomber = Thread.new{place_bomb}	
	end

	def close_socket
		@subscriber.close
		#finalize	
	end

	def listen
		while @alive do 
  			s = ''
  			@subscriber.recv_string(s,ZMQ::NOBLOCK)
  			if s != ''
				#puts s 
				read_msg(s)
			end
			sleep 0.05
  		end
	end

	def next_move
		while @alive 
			count = @move_list.count
			if count > 0
				nxt = @move_list[0]
				@move_list.shift
				count > 1 ? warp = true : warp = false
				dest = nxt["data"]["end_field"]
				start = nxt["data"]["start_field"]
				@x = start[0]*20
				@y = start[1]*20
				@gx = start[0]
				@gy = start[1]
				move('right',warp) if dest[0] > start[0] 
				move('left',warp) if dest[0] < start[0] 
				move('down',warp) if dest[1] > start[1] 
				move('up',warp) if dest[1] < start[1]
			else
				sleep 0.1
			end
		end
	end
	
	def place_bomb
		while @alive
			#puts 'ab'
			count = @bomb_list.count 
			if count > 0
				msg = @bomb_list[0]
				nxt = msg["data"]["field"]
				@bomb_list.shift
				#@bomb.plant(@gx,@gy,@id)
				@bomb.plant(nxt[0],nxt[1],@id)
			end
			sleep 0.1
		end
	end

	def move(dir, warp)
		tmp_x = x
		tmp_y = y
		speed = 0.66
		interval = 20
		@moving = true
		unless warp
			10.times do
				case dir
					when 'left' 	then @x -= 2
					when 'right' 	then @x += 2
					when 'up' 		then @y -= 2
					when 'down' 	then @y += 2
				end
				sleep 0.05
			end
		end
		case dir
				when 'left' 	then @gx -= 1
				when 'right' 	then @gx += 1
				when 'up' 		then @gy -= 1
				when 'down' 	then @gy += 1
		end
		@moving = false
		#puts"koniec"
	end

	def read_msg (j_msg)
		msg = JSON.parse j_msg
			if msg["data"]["time_real"] > @last_ping
				@last_ping = msg["data"]["time_real"]
			end
			case msg["data"]["type"]
				when "move_begin"
					@move_list << msg
					@waiting = true
				#when "no_change"
					#puts "nochange"
				when "bomb"
					@bomb_list << msg
				when "die"
					die
				when "ready"
					puts "ready"
				when "listen"
					puts "listen"
			end
	end
	
	def die
		@alive = false
		$window.enemies_count -= 1
		p "Count: #{$window.enemies_count}"
	end
	
	def update
		super
		if $window.enemies_count == 0 and $player.alive and !$window.result
			Chingu::Text.create(:text => "Wygrana", :x=>53, :y=>120, :size=>50, :color => Color::BLACK)
			$window.result = true
		end
	end
	

	def draw
		Image["img/enemy.png"].draw(@x,@y,0) if @alive
	end
	
	def self.finalize
		puts 'tango down' 
		@subscriber.close
	end
end
	

class Bomb < GameObject
	trait :timer
	def initialize(options)
		super
		@planted = false
		@range = Array.new
	end
	
	def plant(pos_x,pos_y,owner,delay = 0)
		unless @planted
			@planted = true
			@x = pos_x * 20
			@y = pos_y * 20
			@gx = pos_x
			@gy = pos_y
			$player.bomb_planted if owner == $player.id
			if delay > 3
				time = 1000
			else
				time = 3000
			end
			after(time){
				explode
				@planted = false
			}
		end
	end
	
	def explode
		#puts 'boom'
		shockwave(@gx,@gy)
		@exploded = true
		after(100) {
			@exploded = false
			@range.delete_if {true}
		}
	end
	
	def shockwave (x,y)
		[[0,0],[1,0],[-1,0],[0,1],[0,-1]].each do |i|
			[1,2].each do |j|
				dx = i[0] * j
				dy = i[1] * j
				#puts "#{dx} - #{dy}"
				
				if $map.obstacle?(x+dx,y+dy)
					if $map.destroy_brick(x+dx,y+dy)
						@range << [x+dx,y+dy]
					end	
					break
				elsif $player.in_range?(x+dx,y+dy)
					$player.kill
					@range << [x+dx,y+dy]
				else
					@range << [x+dx,y+dy]
				end
			end
		end
	end

	def draw
		Image["img/bomb.png"].draw(@x,@y,0) if @planted
		if @exploded
			@range.each do |k|
				Image["img/explosion.png"].draw(20*k[0], 20*k[1],1000)
			end
		end
	end

end

class Block < GameObject
	def initialize(options)
		super
		@x = 20 * options[:x]
		@y = 20 * options[:y]
		@invulnerable = options[:vulnerable]
		if @invulnerable
			@color = Color::WHITE
		else
			@color = Color::GRAY
		end
	end
	
	def invulnerable?
		return @invulnerable
	end
	
	def draw
		if @invulnerable
			Image["img/steel.jpg"].draw(@x,@y,0)
		else
			Image["img/brick.jpg"].draw(@x,@y,0) 
		end
	end
end

class Spawnpoint < GameObject
	def initialize(options)
		super
		@x = 20 * options[:x]
		@y = 20 * options[:y]
		@color = Color::RED
	end
end

class Map 

	def initialize(options)
		p 'new map'
		@name = options[:map]
		@blocks = Hash.new
		load_map
	end
	
	def max_x
		return 16
	end
	
	def max_y
		return 16
	end
	
	def obstacle? (x,y)
		@blocks.include?("#{x}-#{y}".intern)
	end
	
	def destroy_brick(x,y)
		unless @blocks["#{x}-#{y}".to_sym].invulnerable?
			@blocks["#{x}-#{y}".to_sym].destroy
			@blocks.delete("#{x}-#{y}".to_sym)
			return true
		end
		return false
	end
	
	def load_map
		lins 	= open(@name).lines
		lins.each do |x| 
			values = x.split(' ')
			v = false 
			v = true if values[2] == '1' 
			if values[2] == '2'
			Spawnpoint.create(:x => values[0].to_i, :y => values[1].to_i);
			else
				unless @blocks.include?("#{values[0]}-#{values[1]}".intern)
					block = Block.create(:x => values[0].to_i, :y => values[1].to_i, :vulnerable => v);
					@blocks[(values[0]+'-'+values[1]).intern] = block;
				end
			end 
		end
	end
end


Game.new.show